###########
Change Log
###########

# unreleased
************
* Use ska-ser-sphinx-theme for documentation

v0.2.2
******
* No change, moving artefacts to a new repository https://artefact.skao.int/.

v0.2.1
******
* Changed package dependencies:
    - `ska_logging` to `ska_ser_logging`.
    - `ska-skuid` to `ska-ser-skuid`.

v0.2.0
******
Change packaging to use single namespace.
Usage like `from ska.log_transactions import transaction` changes
to `from ska_ser_log_transactions import transaction`.

Release 0.1.0
-------------
First release of Ska Transaction Logging
