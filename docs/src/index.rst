=============================================
SKA Transaction Logging Utility Documentation
=============================================

Description
-----------

A transaction context handler is available to inject ids fetched from the the skuid service into logs.
The transaction id will be logged on entry and exit of the context handler. In the event of an exception,
the transaction id will be logged with the exception stack trace. The ID generated depends on whether or
not the SKUID_URL environment variable is set.

.. toctree::
  :maxdepth: 1

   Developer Guide<package/guide>
   API<api/transaction>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
