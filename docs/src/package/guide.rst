=============
Example Usage
=============

For Python applications, this is as simple as:

.. code-block:: python

    from ska_ser_log_transactions import transaction

    def command(self, parameter_json):
        parameters = json.reads(parameter_json)
        with transaction('My Command', parameters) as transaction_id:
            # ...
            parameters['transaction_id'] = transaction_id
            device.further_command(json.dumps(parameters))
            # ...

Asynchronous Example
^^^^^^^^^^^^^^^^^^^^
.. code-block:: python

    from ska_ser_log_transactions import async_transaction

    async def command(self, parameter_json):
        parameters = json.reads(parameter_json)
        async with async_transaction('My Command', parameters) as transaction_id:
            # ...
            parameters['transaction_id'] = transaction_id
            await device.further_command(json.dumps(parameters))
            # ...

Custom logger example
^^^^^^^^^^^^^^^^^^^^^
By default the context handler logs to the ``ska.transaction`` logger with default formatting.
A custom logger can be used by passing it in via an optional argument.

.. code-block:: python

    import logging
    from ska_ser_log_transactions import transaction

    custom_logger = logging.getLogger(__name__)

    def command(self, parameter_json):
        parameters = json.reads(parameter_json)
        with transaction('My Command', parameters, logger=custom_logger) as transaction_id:
            # ...
            parameters['transaction_id'] = transaction_id
            device.further_command(json.dumps(parameters))
            # ...

Log messages
^^^^^^^^^^^^
Log message formats:

.. code-block:: rst

    - On Entry:
      - Transaction[id]: Enter[name] with parameters [arguments] marker[marker]
    - On Exit:
      - Transaction[id]: Exit[name] marker[marker]
    - On exception:
      - Transaction[id]: Exception[name] marker[marker]
        -- Stacktrace --

The marker can be used to match entry/exception/exit log messages.

**Example ska formatted logs for successful transaction**

.. code-block:: rst

    1|2020-10-01T12:49:31.119Z|INFO|Thread-210|log_entry|transactions.py#154||Transaction[txn-local-20201001-981667980]: Enter[Command] with parameters [{}] marker[52764]
    1|2020-10-01T12:49:31.129Z|INFO|Thread-210|log_exit|transactions.py#154||Transaction[txn-local-20201001-981667980]: Exit[Command] marker[52764]


**Example ska formatted logs for failed transaction**

.. code-block:: rst

    1|2020-10-01T12:51:35.588Z|INFO|Thread-204|log_entry|transactions.py#154||Transaction[txn-local-20201001-354400050]: Enter[Transaction thread [7]] with parameters [{}] marker[21454]
    1|2020-10-01T12:51:35.598Z|ERROR|Thread-204|log_exit|transactions.py#149||Transaction[txn-local-20201001-354400050]: Exception[Transaction thread [7]] marker[21454]
    Traceback (most recent call last):
      File "python_file.py", line 27, in thread_with_transaction_exception
        raise RuntimeError("An exception has occurred")
    RuntimeError: An exception has occurred
    1|2020-10-01T12:51:35.601Z|INFO|Thread-204|log_exit|transactions.py#154||Transaction[txn-local-20201001-354400050]: Exit[Transaction thread [7]] marker[21454]
