SHELL=/bin/bash
.SHELLFLAGS=-o pipefail -c

NAME=ska-ser-log-transactions

VERSION=$(shell grep -e "^version = s*" pyproject.toml | cut -d = -f 2 | xargs)

# Set the specific environment variables required for pytest
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=.:./src

PYTHON_VARS_AFTER_PYTEST ?= --json-report --json-report-file=build/report.json --junitxml=build/report.xml

PYTHON_SWITCHES_FOR_BLACK = --line-length 120
PYTHON_SWITCHES_FOR_FLAKE8 = --max-line-length 120 --ignore=E203

CI_REGISTRY ?= registry.gitlab.com

-include .make/base.mk
-include .make/docs.mk
-include .make/python.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak
